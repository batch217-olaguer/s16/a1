console.log("hi i'm aL");
/*
	1. Debug the following code to return the correct value and mimic the output.
*/

	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);
		
/*

	2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.

*/
	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	// Minutes in a year:
	let minutesInAYear= (60*24*356);
	console.log("There are "+minutesInAYear+ " minutes in a year.")
	


/*
	3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
*/
	let tempCelsius = 132;
	let tempFahrenheit = tempCelsius* (9/5);
	console.log("132°C when converted to degrees Fahrenheit is " +tempFahrenheit+ "°.");


/*
	4a. Given the values below, identify if the values of the following variable are divisible by 8.
	   -Use a modulo operator to identify the divisibility of the number to 8.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
	   -Log a message in the console if num7 is divisible by 8.
	   -Log the value of isDivisibleBy8 in the console.

*/
	let num7 = 165;
	let dig7 = 8;
	//Log the value of the remainder in the console.
	let num7Rem= num7%dig7;
	if (num7Rem==0) {
		console.log("Has "+num7Rem+ " remainder, therefore " +num7+ " is divisible by " +dig7);
	} else { console.log("The remainder of " +num7+ " divided by " +dig7+ " is: " +num7Rem+ ".");
		console.log("Is num7 divisible by " +dig7+ "?");
		console.log(num7Rem==0);
	}

	//Log the value of isDivisibleBy8 in the console.


/*
	4b. Given the values below, identify if the values of the following variable are divisible by 4.
	   -Use a modulo operator to identify the divisibility of the number to 4.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
	   -Log a message in the console if num8 is divisible by 4.
	   -Log the value of isDivisibleBy4 in the console.

*/
	let num8 = 348;
	let dig8 = 4;
	//Log the value of the remainder in the console.
	let isDivisibleBy4 = num8/4;
	let num8Rem= num8%dig8;
	if (num8Rem==0) {
		console.log("The remainder of " +num8+ " divided by " +dig8+ " is: " +num8Rem+ ".");
		console.log("Is num8 divisible by "+dig8+ "?");
		console.log(num8Rem==0);
	}
	//Log the value of isDivisibleBy4 in the console.
